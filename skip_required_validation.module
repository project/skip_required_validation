<?php

/**
 * @file
 *
 * Provides an API which allows developers to skip the FAPI #required validation procedures.
 */

/**
 * Implementation of hook_help().
 */
function skip_required_validation_help($path, $arg) {
  switch ($path) {
    case 'admin/help#skip_required_validation':
      return '<p>'. t('Provides an API which allows developers to skip the FAPI #required validation procedures.') .'</p>';
      break;
  }
}

/**
 * Implementation of hook_elements().
 */
function skip_required_validation_elements() {
  static $processing = FALSE;

  // Add our process callback to all elements so we can handle the
  // #skip_required_validation action.
  if (!$processing) {
    $processing = TRUE;
    $return = array_fill_keys(array_keys(module_invoke_all('elements')), array('#process' => array('skip_required_validation_process')));
    $processing = FALSE;
    return $return;
  }
}

/**
 * Process callback which handles the required flags.
 */
function skip_required_validation_process($element, $edit, &$form_state, $form) {
  if (!empty($form['#skip_required_validation']) && !empty($element['#required'])) {
    // Make sure that the required flag is not set.
    unset($element['#required']);

    // Add a pre-render function so we can still tell the theme layer
    // to set the fields as required.
    if (!is_array($element['#pre_render'])) {
      $element['#pre_render'] = array();
    }
    $element['#pre_render'][] = 'skip_requried_validation_pre_render';

    // Add an element validation function to handle the actual required value checking.
    if (!is_array($element['#element_validate'])) {
      $element['#element_validate'] = array();
    }
    $element['#element_validate'][] = 'skip_requried_validation_element_validate';
  }
  elseif (
      !empty($form['#skip_required_validation']) && // Validation on the form is not empty,
      in_array($element['#type'], array('submit', 'button')) && // The type is a submit or button.
      in_array($element['#parents'], $form['#skip_required_validation']) && // The current button is in the skip validation array.
      !empty($form_state['clicked_button']) && // A button was clicked.
      $form_state['clicked_button']['#id'] == $element['#id'] // This button was clicked.
  ) {
    $form_state['clicked_button']['#skip_required_validation'] = TRUE;
  }
  return $element;
}

/**
 * Pre render callback for allowing the theme to still think the
 * item is required.
 */
function skip_requried_validation_pre_render($element) {
  // Tell the theme this is a required field.
  $element['#required'] = TRUE;
  return $element;
}

/**
 * Element validation callback to act correctly to validation actions.
 */
function skip_requried_validation_element_validate($element, &$form_state, $form) {
  // We only trigger a required error message if the form id is not in the
  // #skip_required_validation property.
  if (empty($form_state['clicked_button']['#skip_required_validation']) && (!count($element['#value']) || (is_string($element['#value']) && strlen(trim($element['#value'])) == 0))) {
    // May also be used in the installer, pre-database setup.
    $t = get_t();

    form_error($element, $t('!name field is required.', array('!name' => $element['#title'])));
  }
}
